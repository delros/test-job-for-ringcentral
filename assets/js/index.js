var RatesInfo = (function () {

  var baseURL, button, select, output, outputTpl,
  
  expandTemplate = function(template, values) {
    return template.replace(
      new RegExp('{{([^}]+?)(?:\\.(UrlEncoded))?}}', 'g'), 
      function(match, key, encode) {
        var value = values[key];
        if (value) {
          switch (encode) {
          case 'UrlEncoded':
            value = encodeURIComponent(value);
            break;
          }
        }
        return value ? value : match;
      }
    );
  },
  
  bindEvent = function(el, event, callback) {
    if (el.addEventListener) {
      el.addEventListener(event, callback, false);
    } else if (el.attachEvent) {
      el.attachEvent('on' + event, function() {
        callback.call(event.srcElement, event);
      });
    }
  },
  
  bindDomLoaded = function(callback) {
    if (window.addEventListener) {
      window.addEventListener('DOMContentLoaded', callback, false);
    } else {
      window.attachEvent('onload', callback);
    }
  }, 
  
  getJSONP = function(params) {
    var script = document.createElement("script"), 
        query = [];

    select.disabled = true;

    for (key in params) {
      query.push(key + '=' + params[key]);
    }

    script.src = baseURL + '?' + query.join('&');
    script.type = 'text/javascript';

    document.body.appendChild(script);
  }


  return {
    cbCountriesLoaded: function(data) {
      var fragment = document.createDocumentFragment(),
          option;

      if (!data.status.success) {
        console.info('Request failed');
        console.log(data.status);
        alert('Request failed. ' + data.status.message);
        return;
      }


      for (key in data.result) {
        option = document.createElement('option');
        if (option.dataset) {
          option.dataset.id = data.result[key].id;
          option.dataset.code = data.result[key].code;
          option.dataset.shortName = data.result[key].shortName;
          option.dataset.alpha2 = data.result[key].alpha2;
          option.dataset.alive = data.result[key].alive;
        } else {
          option.setAttribute('data-id', data.result[key].id);
          option.setAttribute('data-code', data.result[key].code);
          option.setAttribute('data-shortName', data.result[key].shortName);
          option.setAttribute('data-alpha2', data.result[key].alpha2);
          option.setAttribute('data-alive', data.result[key].alive);
        }
        option.innerHTML = data.result[key].name;
        fragment.appendChild(option);
      }

      select.appendChild(fragment.cloneNode(true));
      select.disabled = false;
    },
    cbRatesLoaded: function(data) {
      var fragment = document.createDocumentFragment(),
          table = document.createElement('table'),
          rateSection = document.createElement('div'),
          rateRow = document.createElement('tr'),
          rateCell = document.createElement('td'),
          ratesPrepared = {};

      if (!data.status.success) {
        select.disabled = false;
        console.info('Request failed');
        console.log(data.status);
        alert('Request failed. ' + data.status.message);
        return;
      }
      
      for (var i = 0; i < data.rates.length; i++) {
        for (var j = 0; j < data.rates[i].value.length; j++) {
          for (var v = 0; v < data.rates[i].value[j].length; v++) {
            var buff = data.rates[i].value[j][v],
                phone = buff['areaCode'].toString() + (buff.phonePart ? buff.phonePart.toString() : ''),
                rate = '$' + buff.rate;
            
            if (!ratesPrepared[buff.type]) {
              ratesPrepared[buff.type] = {
                'code': [], 
                'ratePerMin': []
              };
            }
            
            if (ratesPrepared[buff.type]['code'].indexOf(phone) == -1) {
              ratesPrepared[buff.type]['code'].push(phone);
            } 
            if (ratesPrepared[buff.type]['ratePerMin'].indexOf(rate) == -1) {
              ratesPrepared[buff.type]['ratePerMin'].push(rate);
            }
          }
        }

        console.log(ratesPrepared);

        table.innerHTML = '<tr>'
        + '  <th width="20%">Type</th>'
        + '  <th width="50%">Code</th>'
        + '  <th width="30%">Rate per minute</th>'
        + '</tr>'; 

        for (key in ratesPrepared) {
          var row = rateRow.cloneNode(true),
              cellType = rateCell.cloneNode(true),
              cellCode = rateCell.cloneNode(true),
              cellRate = rateCell.cloneNode(true);
          
          cellType.innerHTML = key;
          cellCode.innerHTML =  ratesPrepared[key]['code'].join(', ');
          cellRate.innerHTML =  ratesPrepared[key]['ratePerMin'].join(', '); 

          row.appendChild(cellType);
          row.appendChild(cellCode);
          row.appendChild(cellRate);

          table.appendChild(row);
        }

        rateEl = rateSection.cloneNode(true);
        rateEl.innerHTML = expandTemplate(outputTpl, {
          'countryName': data.rates[i].key.name,
          'ratesTable': table.outerHTML
        });
        fragment.appendChild(rateEl);
      }

      select.disabled = false;
      output.innerHTML = '';
      output.appendChild(fragment.cloneNode(true));
    },
    init: function(params) {
      bindDomLoaded(function() {
        baseURL = params['baseURL'];
        button = document.getElementById(params['ButtonID']);
        select = document.getElementById(params['SelectID']);
        output = document.getElementById(params['OutputID']);

        outputTpl = output.innerHTML;
        output.innerHTML = '';

        if (!select || !button) {
          console.info('Select or button ID elements are missed');
          return;
        }      

        bindEvent(button, 'click', function() {
          var optionSelected = select[select.selectedIndex],
              dataset = optionSelected.dataset ? optionSelected.dataset : {
                'id': optionSelected.getAttribute('data-id')
              };

          getJSONP({
            'cmd': 'getInternationalRates',
            'typeResponse': 'json',
            'param[internationalRatesRequest][countryId]': dataset.id,
            'param[internationalRatesRequest][brandId]': 1210,
            'param[internationalRatesRequest][tierId]': 3311,
            'callback': 'RatesInfo.cbRatesLoaded'
          });
        });

        getJSONP({
          'cmd': 'getCountries',
          'typeResponse': 'json',
          'callback': 'RatesInfo.cbCountriesLoaded'
        });

      });
    }
  };

})();

RatesInfo.init({
  'baseURL': 'http://www.ringcentral.com/api/index.php',
  'SelectID': 'countriesList',
  'ButtonID': 'getRates',
  'OutputID': 'listRates'
});